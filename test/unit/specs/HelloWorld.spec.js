import Vue from 'vue'
import HelloWorld from '@/components/HelloWorld'

describe('HelloWorld.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(HelloWorld)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.hello h1').textContent)
    .to.equal('Welcome to Your Vue.js App')
    // 在状态改变后和断言 DOM 更新前等待一刻
    vm.$nextTick(() => {
      expect(vm.$el.querySelector('.hello h1').textContent)
      .to.equal('ok')
    })
  })
})

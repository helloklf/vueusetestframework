// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage
var axios = require('axios')

module.exports = {
  'default e2e tests': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL

    browser
      .url(devServer)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('.hello')
      .assert.containsText('h1', 'ok')
      .assert.elementCount('img', 1)
      .end()
  },

  'api-login-test': function (browser) {
    axios.options('http://test.www.phone580.com:8000//fbsapi/api/user/login?password=12345678&userName=13288076262').then(response => {
      var headers = response.headers
      const devServer = browser.globals.devServerURL
      var originControl = headers['access-control-allow-origin']
      if (originControl !== undefined && (originControl === '*' || originControl.startsWith(devServer))) {
        browser.end()
      } else {
        browser.expect(devServer).to.equal(originControl)
      }
    }).catch(error => {
      console.log(error)
    })
  }
}
